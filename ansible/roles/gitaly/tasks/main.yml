---
- name: Setup GitLab config file
  template:
    src: templates/gitaly.gitlab.rb.j2
    dest: /etc/gitlab/gitlab.rb
  register: result
  retries: 3
  until: result is success
  tags: reconfigure

- name: Check if custom config exists
  stat:
    path: "{{ gitaly_custom_config_file }}"
  delegate_to: localhost
  become: false
  tags: reconfigure
  register: gitaly_custom_config_file_path

- name: Setup Custom Config
  template:
    src: "{{ gitaly_custom_config_file }}"
    dest: "/etc/gitlab/gitlab.gitaly.custom.rb"
    mode: 0644
  tags: reconfigure
  when: gitaly_custom_config_file_path.stat.exists

- name: Copy over any Custom Files
  copy:
    src: "{{ item.src_path }}"
    dest: "{{ item.dest_path }}"
    mode: "{{ item.mode if item.mode is defined else 'preserve' }}"
  loop: "{{ gitaly_custom_files_paths }}"
  tags: reconfigure

- name: Propagate Secrets if existing
  include_role:
    name: common
    tasks_from: secrets
  tags:
    - reconfigure
    - secrets

- name: Reconfigure Gitaly
  command: gitlab-ctl reconfigure
  tags: reconfigure

- name: Create Default Gitaly Sharded directory
  file:
    path: "{{ gitaly_sharded_storage_path }}/default/repositories"
    state: directory
    mode: '2770'
    owner: git
    group: git
  when:
    - "'gitaly_primary' in group_names"
    - "'praefect' not in groups"
  tags: reconfigure

- name: Create Additional Gitaly Sharded directories
  file:
    path: "{{ gitaly_sharded_storage_path }}/storage{{ gitaly_number }}/repositories"
    state: directory
    mode: '2770'
    owner: git
    group: git
  when: "'praefect' not in groups"
  tags: reconfigure

- name: Propagate Secrets if new
  include_role:
    name: common
    tasks_from: secrets
  vars:
    gitlab_secrets_reconfigure: true
  tags:
    - reconfigure
    - secrets

- name: Restart Gitaly
  command: gitlab-ctl restart
  register: result
  retries: 2
  until: result is success
  tags:
    - reconfigure
    - restart

- name: Create skip-auto-reconfigure file
  file:
    path: /etc/gitlab/skip-auto-reconfigure
    state: touch
    mode: u=rw,g=r,o=r

- name: Run Custom Tasks
  block:
    - name: Check if Custom Tasks file exists
      stat:
        path: "{{ gitaly_custom_tasks_file }}"
      register: gitaly_custom_tasks_file_path
      delegate_to: localhost
      become: false

    - name: Run Custom Tasks
      include_tasks:
        file: "{{ gitaly_custom_tasks_file }}"
        apply:
          tags: custom_tasks
      when: gitaly_custom_tasks_file_path.stat.exists
  tags: custom_tasks
